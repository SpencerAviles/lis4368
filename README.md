> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 Advanced Web Applications Development

## Spencer Aviles

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Write Hello World Java Servlet
    - Write Query Servlet
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create SQL Database with populated tables
    - Include .mwb and .sql files in ReadMe
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server-Side data validation
    - Include valid submission
    - Skillset 10 - Java: Count Characters
    - Skillset 11 - Java: Write/Read Count Words
    - Skillset 12 - Java: ASCII App
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Basic Server-Side validation
    - Include changes to local SQL table
    - Skillset 13 - Java: Number Swap App
    - Skillset 14 - Java: Largest of Three Numbers
    - Skillset 15 - Java: Simple Calculator Using Methods
    
6. [P1 README.md](p1/README.md "MY P1 README.md file")
    - Add jQuery validation to p1/index.jsp
    - Use regexp to only allow appropriate characters for each control
    - Set a min and max to validate the user entries 
    
7. [P2 README.md](p2/README.md "MY P2 README.md file")
    -  Create a MVC framework with basic client, server-side validation
    - Allows user to insert, update, and delete customers from the database
    - Be able to display all customers in a list
    - Screenshots of working web pages and MySQL table entries



