import java.util.Scanner;
public class ss10
{
    public static void main(String[]args)
    {
	System.out.println("Program counts number and types of characters");
	System.out.println("Author: Spencer Aviles");

	System.out.println("\nPlease enter string: ");

	int letter = 0;
	int space = 0;
	int num = 0;
	int other = 0;
	String str = "";

	Scanner input = new Scanner(System.in);
	str = input.nextLine();

	char[] ch = str.toCharArray();

	for(int i = 0; i < str.length(); i++)
	    {
		if(Character.isLetter(ch[i]))
		    {
			letter++;
		    }
		else if(Character.isDigit(ch[i]))
		    {
			num++;
		    }
		else if (Character.isSpaceChar(ch[i]))
		    {
			space++;
		    }
		else
		    {
			other++;
		    }
	    }
		System.out.println("\nYour string: \"" + str + "\" has the following number and types of characters:");
		System.out.println("letter(s): " + letter);
		System.out.println("space(s): " + space);
		System.out.println("number(s): " + num);
		System.out.println("other character(s): " + other);
    }
}
				  
