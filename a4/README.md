> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Assignment 4 Requirements:

1. Create server-side data validation page
2. Include valid entry page
3. Java Skillsets 10-12

#### README.md file should include the following items:

* Screenshot of unfilled boxes
* Screenshots of valid entry
* Screenshots of Skillsets 10-12
    
#### Assignment Screenshots:

*Screenshots of opening page*

![home](home.png)	![red](fill.png)


*Screenshot of Valid Submission*

![Valid](valid.png)


*Skillset 10*

![SS10](ss10.png)


*Skillset 11*

![SS11](ss11.png)


*Skillset 12*

![Part 1](ss12_1.png)		![Part 2](ss12_2.png)