> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket 
2. JDK Java installation
3. Tomcat installation
4. Chapter questions

#### README.md file should include the following items:

* Screenshot of Java hello
* Screenshot of running http://localhost:9999
* Screenshot of local lis4368 web app: http://localhost:9999/lis4368/
* git commands w/ short descriptions
* Bitbucket repo links
  a) This assignment
  b) The completed tutorial repo (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository or reinitializes an existing one
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](java_hello.png)

*Screenshot of localhost running:
![Tomcat Localhost:9999 Screenshot](localhost999.png)

*Screenshots of LIS 4368 index.jsp
![Homepage Screenshot](pic1_ofwebsite.png)

*Screenshot of A1 index.jsp
![A1 Screenshot](pic2_ofwebsite.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/SpencerAviles/bitbucketstationlocations/src/master)
