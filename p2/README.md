> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Project 2 Requirements:

1. Create a MVC framework with basic client, server-side validation
2. Allows user to insert, update, and delete customers from the database
3. Be able to display all customers in a list 

#### README.md file should include the following items:

* Screenshot of Insert confirmation and table 
* Screenshot of Update confirmation and table
* Screenshot of Delete confirmation
* Screenshots of MySQL customer table entries

#### Assignment Screenshots:

*Insert Page*

![Insert](add.png)	![Confirm](confirm.png)

![Table Confirm](addtable.png)

*Updated Page*

![Update](update.png)


![Update Table](updatetable.png)

*Delete Page*

![Delete](delete.png)


![Delete Table](deletetable.png)

*MySQL Table Screenshots*

*Initial*

![Initial](initial.png)

*Add, Update, and Delete MySQL Tables*

![Post tables](post.png)