> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Assignment 3 Requirements:

1. Create a Database including the following:
    - 3 tables (pets, petstore, customers)
    - Have a 10 records per tables

#### README.md file should include the following items:

* Screenshots of ERD
* Screenshot of a3/index.jsp
* Links to the following files:
    1. a3.mwb
    2. a3.sql
    
#### Assignment Screenshots:

*ERD Screenshot*
![A3 Erd](erd_a3.png)

*Populated Tables*

*Pet Store*
![Pet Store](petstore.png)

*Customers*
![Customers](customers.png)

*Pets*
![Pets](pets.png)

*Assignment 3 Index*
![A3 Index](a3_index.png)

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/lis4368_a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/lis4368_a3.sql "A3 SQL Script")