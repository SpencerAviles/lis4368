> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Project 1 Requirements:

1. Create a client-side validation using bootstrap
2. Must require user to enter valid entries for all fields
3. Must have "fa fa-check" or "fa fa-times" to show for user entries

#### README.md file should include the following items:

* Screenshot of Main/Splash page
* Screenshot of Failed Validation
* Screenshot of Passed Validation

#### Assignment Screenshots:

*Main Page*

![main page](mainpage.png)

*Failed Validation*

![failed](failed.png)

*Passed Validation*

![passed](passed.png)
