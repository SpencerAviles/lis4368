> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Assignment 2 Requirements:

1. Write a "Hello-world" Java Servlet
2. Write a Database Servlet
3. Compile both Servlets
4. Create a database on local server

#### README.md file should include the following items:

* Screenshots of http://localhost:9999/hello
* Screenshots of http://localhost:9999/hello/index.html
* Screenshots of http://localhost:9999/hello/sayhello
* Screenshots of http://localhost:9999/hello/querybook.html

#### Assignment Screenshots:

*http://localhost:9999/hello*

![Screenshot](hello.png)

*http://localhost:9999/hello/index.html*

![Screenshot](index_file.png)

*http://localhost:9999/hello/sayhello*

![Screenshot](sayhello.png)

*http://localhost:9999/hello/querybook.html (Box Selected)*

![Screenshot](query_home.png)

*Querybook after you hit search*

![Screenshot](finished_search.png)

*Screenshots of A2 on localhost*

![Screenshot](a2_web1.png) ![Screenshot](a2_web2.png)