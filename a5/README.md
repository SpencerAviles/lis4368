> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Spencer Aviles 

### Assignment 5 Requirements:

1. Create server-side data validation page
2. Include valid entry page
3. Update the SQL customer table on localhost
3. Java Skillsets 13-15

#### README.md file should include the following items:

* Screenshot of unfilled boxes
* Screenshot of valid entry
* Screenshot of SQL tables
* Screenshots of Skillsets 13-15
    
#### Assignment Screenshots:

*Screenshot of unfilled page*

![Pre](pre.png)


*Screenshot of submitted data*

![Post](post.png)


*Screenshot of SQL table change*

![SQL](tables.png)


*Skillsets 13, 14, and 15*

![SS13](ss13.png)

![SS14](ss14.png)

![SS15](ss15.png)