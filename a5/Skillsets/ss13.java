import java.util.Scanner;

class ss13
{
    public static void main(String args[])
    {
	System.out.println("Program swaps 2 integers");
	System.out.println("Author: Spencer Aviles");
	System.out.println();

	int num1, num2, temp;
	Scanner input = new Scanner(System.in);

	System.out.print("Please enter first number: ");
	while (!input.hasNextInt())
	       {
		   System.out.println("Not valid integer!\n");
		   input.next();
		   System.out.println("Please try again. Enter first number: ");
	       }
	       num1 = input.nextInt();

	       System.out.print("\nPlease enter second number: ");
	       while (!input.hasNextInt())
	       {
		   System.out.println("Not valid integer!\n");
		   input.next();
		   System.out.println("Please try again. Enter second number: ");
	       }
	       num2 = input.nextInt();

	       System.out.println("\nBefore Swapping\nnum1 = " + num1 + "\nnum2 = " + num2);
	       temp = num1;
	       num1 = num2;
	       num2 = temp;

	       System.out.println("\nAfter Swapping \nnum1 = " + num1 + "\nnum2 = " + num2);
    }
}
