import java.util.Scanner;

public class ss14
{
    public static void main(String args[])
    {
	System.out.println("Program evaluates largest of 3 integers");
	System.out.println("Author: Spencer Aviles");

	int num1, num2, num3;
       	Scanner scnr = new Scanner(System.in);

	System.out.print("Please enter first number: ");
	while (!scnr.hasNextInt())
	    {
		System.out.println("Not a valid integer!");
		scnr.next();
		System.out.print("Please try again.\nEnter first number: ");
	    }
	num1 = scnr.nextInt();
	System.out.println();

       	System.out.print("Please enter second number: ");
	while (!scnr.hasNextInt())
	    {
		System.out.println("Not a valid integer!");
		scnr.next();
		System.out.print("Please try again.\nEnter second number: ");
	    }
	num2 = scnr.nextInt();
	System.out.println();

       	System.out.print("Please enter third number: ");
	while (!scnr.hasNextInt())
	    {
		System.out.println("Not a valid integer!");
		scnr.next();
		System.out.print("Please try again.\nEnter third number: ");
	    }
	num3 = scnr.nextInt();
	System.out.println();

	if (num1 >= num2 && num1 >= num3)
	    {
		System.out.println("First number, " + num1 + ", is the largest.");
	    }
	else if (num2 >= num1 && num2 >= num3)
	    {
		System.out.println("Second number, " + num2 + ", is the largest.");
	    }
	else
	    {
		System.out.println("Third number, " + num3 + ", is the largest.");
	    }
    }
}
